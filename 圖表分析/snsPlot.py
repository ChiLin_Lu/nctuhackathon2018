import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

file = "D://Code/Python_Code/pm25Hackathon2018/pm25.csv"
data = pd.read_csv(file)

def distplot(data,xlabel='pm2.5'):
    sns.set()
    snsplot = sns.distplot(data)
    snsplot.set(xlabel='pm2.5')
    fig = snsplot.get_figure()
    fig.savefig('output.png')
    return
