import json
import csv


inFile = "0694317100256465"
outFile = "airData.csv"

with open(inFile) as inFile:
    data = json.loads(inFile.read())
    f = csv.writer(open(outFile, "wb+"))
    f.writerow(["date", "pm2.5"])
    for item in data:
        if item.sid == 'pm2.5':
            f.writerow(
                        item["time"]["$date"],
                        item["value"])

    f.close()