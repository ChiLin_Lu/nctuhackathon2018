import csv


def listToCsv(col,inFile,outFile):

    i = 0
    k = 0
    with open(inFile, 'r') as inFile:
        with open(outFile, 'w') as outFile:
            outFile.write("date,"+col + "\n")
            for row in inFile:
                if col not in row:
                    continue
                else:
                    x = row.split('"')
                    col_value = x[21]
                    date = x[27][:4] + x[27][5:7] + x[27][8:10]
                    time = x[27][11:13] + x[27][14:16] + x[27][17:19]
                    text_tuple = date + time,"," , col_value
                    text_str = ''.join(text_tuple) + "\n"
                    outFile.write(text_str)
                    print(text_str)
                    # i += 1
                    # if i>2:
                    #     break


def pm25_timeTopm25(inFile,outFile):
    with open(inFile, 'r') as inFile:
        with open(outFile,'w') as outFile:
            for row in inFile:
                outFile.write(row.split(',')[1])
                print(row.split(','))


inFile = "0694317100256465"
outFile = "airData.csv"

# listToCsv('pm2.5', inFile, outFile)
pm25_timeTopm25('airData.csv', 'pm25.csv')